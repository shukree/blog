@extends('blogs.app')

@section('content')

<div class="container">
    <div class="justify-content-center">
        <form method="POST" action="{{ route('blogs.store')}}">
            @csrf
            <h1> New Blog Post</h1>
            <div class="row mt-4">
                <div class="col-6">
                <label for="WriterName">Writer Name</label>
                <input type="text" class="form-control" id="WriterName" name="name" required>
                </div>

                <div class="col-6">
                <label for="title">Post Title</label>
                <input type="text" class="form-control" name="title" required>
                </div>
            
            </div>

                <div class="mt-4">
                    <div class="form-group col-md-12 mt-2">
                        <label for="post">Blog Post</label>
                        <textarea class="form-control" name="post" rows="10" required></textarea>
                    </div>
                </div>

            <button type="submit" class="btn btn-info mt-4 col-4">Submit</button>
            
        </form>
    </div>
</div>


@endsection

