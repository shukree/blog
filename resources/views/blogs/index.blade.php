@extends('blogs.app')

@section('content')

<div class="container">
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show ">
            <p>{{ $message }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    {{-- a foreach loop is run through to get all the blog posts found and accessing the title,name, post and last updated time --}}
    @foreach ($allBlogs as $blog)
    <div class="row mt-4">

        <article>
            <h2 class="display-5">{{ $blog->title }}</h2>
            
            <span class="text-muted"><a>Written By: {{ $blog->name}} , Last Updated at {{$blog->updated_at->format('H:i:s A')}}</a></span>
            

            <div class="row mt-4">
                <p>{{ Str::limit($blog->description, 500, ' ...') }}</p>
            </div>

            <form action="{{ route('blogs.show', $blog->id) }}" method="GET">
                @csrf
            <button class="btn btn-primary">Read More</button>
            </form> {{-- the button is routed to show the specific blog post in detail --}}

        </article>
        
    </div>
    @endforeach

</div>


@endsection