<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>Blog</title>

</head>


<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        
        <div class="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
            <div class="d-flex flex-grow-1">
                <span class="w-100 d-lg-none d-block">
                    <!-- hidden spacer to center brand on mobile --></span>
                <a class="navbar-brand d-none d-lg-inline-block ms-4" href="#"> My Blog </a>
                <a class="navbar-brand-two mx-auto d-lg-none d-inline-block" href="#"></a>
                <div class="w-100 text-right">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
            <ul class="navbar-nav ms-auto flex-nowrap me-4">
                <li class="nav-item me-2">
                    <a href="/blogs" class="nav-link menu-item nav-active">Home</a>
                </li>
                <li class="nav-item">
                    <a href="/blogs/create" class="nav-link menu-item">Create Blog Post</a>
                </li>
            </ul>
        </div>
    </nav>
    
    <main class="py-4">
        @yield('content')
    </main>


<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}" defer></script>

</body>
</html>