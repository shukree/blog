@extends('blogs.app')

@section('content')

<div class="container">
    <div class="row mt-4">

        <article>
            <h2 class="display-5">{{ $blog->title }}</h2>
            
            <span class="text-muted"><a>Written By: {{ $blog->name}} , Last Updated at {{$blog->updated_at->format('H:i:s A')}}</a></span>
            

            <div class="row mt-4">
                <p>{{ $blog->description }}</p>
            </div>

            <form action="{{ route('blogs.destroy', $blog->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <a class="btn btn-warning" role="button" href="{{ route('blogs.edit', $blog->id) }}" > Edit Blog Post</a>
                <button type="submit" class="btn btn-danger ms-4">Delete Blog Post</button>
            </form>

        </article>
        
    </div>

</div>


@endsection