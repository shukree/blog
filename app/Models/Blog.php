<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title',
        'description',
        'name',
     ];

     protected $dates = [
         'updated_at',
     ];//eloquent for converting to carbon
}
